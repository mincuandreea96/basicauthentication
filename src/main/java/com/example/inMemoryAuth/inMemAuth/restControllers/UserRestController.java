package com.example.inMemoryAuth.inMemAuth.restControllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.inMemoryAuth.inMemAuth.model.User;
import com.example.inMemoryAuth.inMemAuth.services.UserService;
import com.example.inMemoryAuth.inMemAuth.model.*;

@RestController
public class UserRestController {

	
	@Autowired
	private UserService userService;
	
	
	@GetMapping("/save-user")
	public String saveUser(@RequestParam String username,@RequestParam String firstname,
			@RequestParam String lastname,@RequestParam int age,@RequestParam String password,@RequestParam String email,int active){
		User user=new User(username, firstname, lastname, age, password, email,active);
        userService.saveUser(user);
		return "User Saved";
	}

	public void  updateUserDetails(){
		
	}
	
}
